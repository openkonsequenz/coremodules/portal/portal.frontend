/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
export class Globals {
  static FRONTEND_VERSION = '2.0.0';
  static SESSION_TOKEN_TAG = 'X-XSRF-TOKEN';
  static BASE_PORTAL_URL = '/portal/rest/beservice';
  static ACCESS_TOKEN = 'ACCESS_TOKEN';
  static LOCALSTORAGE_SESSION_ID = '/elogbook/session-id';
}
