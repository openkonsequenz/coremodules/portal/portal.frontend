/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { JWT_OPTIONS, JwtHelperService } from '@auth0/angular-jwt';
import { Globals } from '@common/globals';
import {
  JWT_ACCESS_TOKEN_HUGO,
  JWT_ACCESS_TOKEN_HUGO_EXPIRING_IN_2009,
  JWT_ACCESS_TOKEN_HUGO_EXPIRING_IN_2199,
} from 'app/testing/jwt-token';
import { SessionContext } from './session-context';

describe('SessionContext', () => {
  let service: SessionContext;
  let router: Router;
  let jwtHelper: JwtHelperService;

  beforeEach(() => {
    const routerSpy = jasmine.createSpyObj('Router', ['navigate']);

    TestBed.configureTestingModule({
      providers: [
        SessionContext,
        { provide: Router, useValue: routerSpy },
        JwtHelperService,
        { provide: JWT_OPTIONS, useValue: {} },
      ],
    });

    service = TestBed.inject(SessionContext);
    router = TestBed.inject(Router);
    jwtHelper = TestBed.inject(JwtHelperService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('#getCurrSessionId should return session id from local storage', () => {
    const testSessionId = 'testSessionId';
    localStorage.setItem(Globals.LOCALSTORAGE_SESSION_ID, testSessionId);
    expect(service.getCurrSessionId()).toBe(testSessionId);
    localStorage.removeItem(Globals.LOCALSTORAGE_SESSION_ID);
  });

  it('#getCurrSessionId should return null if no session id in local storage', () => {
    localStorage.removeItem(Globals.LOCALSTORAGE_SESSION_ID);
    expect(service.getCurrSessionId()).toBe('');
  });

  it('#setCurrSessionId should set session id to local storage', () => {
    const testSessionId = 'testSessionId';
    service.setCurrSessionId(testSessionId);
    expect(localStorage.getItem(Globals.LOCALSTORAGE_SESSION_ID)).toBe(testSessionId);
    localStorage.removeItem(Globals.LOCALSTORAGE_SESSION_ID);
  });

  it('#clearStorage should clear local storage', () => {
    localStorage.setItem('testKey', 'testValue');
    service.clearStorage();
    expect(localStorage.getItem('testKey')).toBeNull();
  });

  it('#getAccessToken should return access token from local storage', () => {
    const testAccessToken = 'testAccessToken';
    localStorage.setItem(Globals.ACCESS_TOKEN, testAccessToken);
    expect(service.getAccessToken()).toBe(testAccessToken);
    localStorage.removeItem(Globals.ACCESS_TOKEN);
  });

  it('#getAccessToken should return null if no access token in local storage', () => {
    localStorage.removeItem(Globals.ACCESS_TOKEN);
    expect(service.getAccessToken()).toBeNull();
  });

  it('#setAccessToken should set access token to local storage', () => {
    const testAccessToken = 'testAccessToken';
    service.setAccessToken(testAccessToken);
    expect(localStorage.getItem(Globals.ACCESS_TOKEN)).toBe(testAccessToken);
    localStorage.removeItem(Globals.ACCESS_TOKEN);
  });

  it('#getAccessTokenDecoded should return decoded access token', () => {
    const testAccessToken = JWT_ACCESS_TOKEN_HUGO;
    const jwtHelper = new JwtHelperService();
    const decodedToken = jwtHelper.decodeToken(testAccessToken);
    spyOn(jwtHelper, 'decodeToken').and.returnValue(decodedToken);
    service.setAccessToken(testAccessToken);
    const decoded = service.getAccessTokenDecoded();
    expect(decoded?.name).toEqual(decodedToken.name);
    localStorage.removeItem(Globals.ACCESS_TOKEN);
  });

  it('should navigate to login and throw error when getAccessToken returns null', () => {
    spyOn(service, 'getAccessToken').and.returnValue(null);
    expect(() => service.getAccessTokenDecoded()).toThrowError('No access token found');
    expect(router.navigate).toHaveBeenCalledWith(['/login']);
  });

  it('should navigate to login and throw error when decoded.exp is greater than nowTime', () => {
    spyOn(service, 'getAccessToken').and.returnValue(JWT_ACCESS_TOKEN_HUGO_EXPIRING_IN_2199);
    spyOn(jwtHelper, 'decodeToken');
    expect(() => service.getAccessTokenDecoded()).toThrowError('Decoded Access Token is undefined or expired');
    expect(router.navigate).toHaveBeenCalledWith(['/login']);
  });

  it('should return a JwtPayload object with roles from realm_access and resource_access when decoded is not null, decoded.exp is less than nowTime, and decoded has realm_access and resource_access properties', () => {
    spyOn(service, 'getAccessToken').and.returnValue(JWT_ACCESS_TOKEN_HUGO_EXPIRING_IN_2009);
    const jwtPayload = service.getAccessTokenDecoded();
    expect(jwtPayload?.name).toEqual('Hugo Boss');
    expect(jwtPayload?.roles).toEqual([
      'elogbook-normaluser',
      'uma_authorization',
      'manage-account',
      'manage-account-links',
      'view-profile',
    ]);
  });
});
