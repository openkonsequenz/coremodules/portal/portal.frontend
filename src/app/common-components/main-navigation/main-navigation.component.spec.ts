/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { SessionContext } from '@common/session-context';
import { MainNavigationComponent } from './main-navigation.component';

describe('MainNavigationComponent', () => {
  let component: MainNavigationComponent;
  let fixture: ComponentFixture<MainNavigationComponent>;
  let sessionContext: SessionContext;
  let dialog: MatDialog;
  let router: Router;

  beforeEach(async () => {
    const dialogMock = {
      open: jasmine.createSpy('open'),
    };

    const routerMock = {
      navigate: jasmine.createSpy('navigate'),
    };

    await TestBed.configureTestingModule({
      declarations: [MainNavigationComponent],
      providers: [
        { provide: MatDialog, useValue: dialogMock },
        { provide: Router, useValue: routerMock },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(MainNavigationComponent);
    component = fixture.componentInstance;
    sessionContext = TestBed.inject(SessionContext);
    dialog = TestBed.inject(MatDialog);
    router = TestBed.inject(Router);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open dialog when openDialogLogout is called', () => {
    component.openDialogLogout();
    expect(dialog.open).toHaveBeenCalled();
  });

  it('should navigate to overview when navigate is called and access token is present', () => {
    spyOn(sessionContext, 'getAccessToken').and.returnValue('token');
    component.navigate();
    expect(router.navigate).toHaveBeenCalledWith(['/overview']);
  });

  it('should navigate to login when navigate is called and access token is not present', () => {
    spyOn(sessionContext, 'getAccessToken').and.returnValue(null);
    component.navigate();
    expect(router.navigate).toHaveBeenCalledWith(['/login']);
  });
});
