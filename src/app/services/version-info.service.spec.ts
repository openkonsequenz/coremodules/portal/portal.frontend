/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { EventEmitter } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { Globals } from '@common/globals';
import { SessionContext } from '@common/session-context';
import { VersionInfo } from '@model/version-info';
import { VersionInfoService } from './version-info.service';

describe('VersionInfoService', () => {
  let service: VersionInfoService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    const sessionContextMock = {
      getAccessToken: jasmine.createSpy('getAccessToken').and.returnValue('token'),
      getCurrSessionId: jasmine.createSpy('getCurrSessionId').and.returnValue('CurrSessionId'),
      centralHttpResultCode$: new EventEmitter<number>(),
    };

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [VersionInfoService, { provide: SessionContext, useValue: sessionContextMock }],
    });

    service = TestBed.inject(VersionInfoService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify(); // Ensure that there are no outstanding requests
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should load backend server info', done => {
    const mockVersionInfo: VersionInfo = {
      backendVersion: '2.2',
      dbVersion: '3.3',
      frontendVersion: '4.4',
    };

    service.loadBackendServerInfo().subscribe(versionInfo => {
      expect(versionInfo).toEqual(mockVersionInfo);
      done();
    });

    const req = httpMock.expectOne(Globals.BASE_PORTAL_URL + '/versionInfo');
    expect(req.request.method).toBe('GET');
    req.flush(mockVersionInfo);
  });
});
