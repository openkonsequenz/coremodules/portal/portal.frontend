/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Globals } from '@common/globals';
import { SessionContext } from '@common/session-context';
import { Observable, throwError as observableThrowError } from 'rxjs';
import * as uuid from 'uuid';

@Injectable()
export class BaseHttpService {
  protected getBaseUrl(): string {
    return Globals.BASE_PORTAL_URL;
  }

  protected createCommonHeaders(sessionContext: SessionContext): HttpHeaders {
    let headers = new HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + sessionContext.getAccessToken(),
      'unique-TAN': uuid.v4(),
    });

    const currSessionId = sessionContext.getCurrSessionId();
    if (currSessionId) {
      headers = headers.set(Globals.SESSION_TOKEN_TAG, currSessionId);
    }

    return headers;
  }

  protected extractData<T>(res: HttpResponse<T>, sessContext: SessionContext): T {
    // Let the interested 'people' know about our result
    sessContext.centralHttpResultCode$.emit(res.status);

    if (res.status !== 302 && (res.status < 200 || res.status >= 300)) {
      throw new Error('Bad response status: ' + res.status);
    }

    return res.body as T;
  }

  protected extractSessionId(headers: HttpHeaders, sessionContext: SessionContext): void {
    if (headers !== null) {
      if (headers.has(Globals.SESSION_TOKEN_TAG)) {
        const sessionTokenTag = headers.get(Globals.SESSION_TOKEN_TAG);
        if (sessionTokenTag) {
          sessionContext.setCurrSessionId(sessionTokenTag);
        }
      }
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  protected handleErrorPromise(error: any, sessContext: SessionContext): Observable<any> {
    // In a real-world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof HttpResponse) {
      errMsg = `${error.status} - ${error.statusText || ''}`;
      try {
        const body = error.body || {};
        const err = body.error || JSON.stringify(body);
        errMsg += ` ${err}`;
      } catch (exc) {
        // eslint-disable-next-line no-console
        console.error(exc);
      }
      // Let the interested 'people' know about our result
      sessContext.centralHttpResultCode$.emit(error.status);
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    // eslint-disable-next-line no-console
    console.error(errMsg);

    return observableThrowError(errMsg);
  }
}
