/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { EventEmitter } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { Globals } from '@common/globals';
import { SessionContext } from '@common/session-context';
import { BaseHttpService } from './base-http.service';

describe('BaseHttpService', () => {
  let service: BaseHttpService;
  let httpMock: HttpTestingController;
  let sessionContext: SessionContext;

  beforeEach(() => {
    const sessionContextMock = {
      getAccessToken: jasmine.createSpy('getAccessToken').and.returnValue('token'),
      getCurrSessionId: jasmine.createSpy('getCurrSessionId').and.returnValue('mockSessionId'),
      setCurrSessionId: jasmine.createSpy('setCurrSessionId'),
      centralHttpResultCode$: new EventEmitter<number>(), // Add this line
    };

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [BaseHttpService, { provide: SessionContext, useValue: sessionContextMock }],
    });

    service = TestBed.inject(BaseHttpService);
    httpMock = TestBed.inject(HttpTestingController);
    sessionContext = TestBed.inject(SessionContext);
  });

  afterEach(() => {
    httpMock.verify(); // Ensure that there are no outstanding requests
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should create common headers', () => {
    const mockSessionContext = {
      getAccessToken: (): string => 'mockToken',
      getCurrSessionId: (): string => 'mockSessionId',
    };
    const headers = (service as any).createCommonHeaders(mockSessionContext);

    expect(headers.get('Accept')).toEqual('application/json');
    expect(headers.get('Content-Type')).toEqual('application/json');
    expect(headers.get('Authorization')).toEqual('Bearer mockToken');
    expect(headers.get('unique-TAN')).toBeDefined();
    expect(headers.get(Globals.SESSION_TOKEN_TAG)).toEqual('mockSessionId');
  });

  it('should extract data from HttpResponse', () => {
    const mockResponse = new HttpResponse({ status: 200, body: { key: 'value' } });
    const data = (service as any).extractData(mockResponse, sessionContext);

    expect(data).toEqual({ key: 'value' });
  });

  it('should extract session id from HttpHeaders', () => {
    const mockHeaders = new HttpHeaders().set(Globals.SESSION_TOKEN_TAG, 'mockSessionId');
    (service as any).extractSessionId(mockHeaders, sessionContext);

    expect(sessionContext.getCurrSessionId()).toEqual('mockSessionId');
  });

  it('should handle error promise', done => {
    const mockError = new HttpResponse({ status: 404, statusText: 'Not Found', body: { error: 'Not Found' } });
    (service as any).handleErrorPromise(mockError, sessionContext).subscribe({
      error: (errMsg: any) => {
        expect(errMsg).toContain('404 - Not Found');
        done();
      },
    });
  });
});
