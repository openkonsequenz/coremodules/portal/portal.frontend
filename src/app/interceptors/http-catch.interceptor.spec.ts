/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { HttpErrorResponse, HttpHandler, HttpRequest } from '@angular/common/http';
import { Router } from '@angular/router';
import { of, throwError } from 'rxjs';
import { HttpCatchInterceptor } from './http-catch.interceptor';

describe('HttpCatchInterceptor', () => {
  let interceptor: HttpCatchInterceptor;
  let router: Router;
  let httpHandler: HttpHandler;

  beforeEach(() => {
    router = jasmine.createSpyObj('Router', ['navigate']);
    httpHandler = jasmine.createSpyObj('HttpHandler', ['handle']);
    interceptor = new HttpCatchInterceptor(router);
  });

  it('should return the result of handle method when it does not throw an error', done => {
    const request = new HttpRequest('GET', 'http://localhost');
    const response = of('response');
    (httpHandler.handle as jasmine.Spy).and.returnValue(response);
    interceptor.intercept(request, httpHandler).subscribe(result => {
      expect(result).toEqual('response');
      done();
    });
  });

  it('should navigate to login and rethrow the error when handle method throws an HttpErrorResponse with status 401', () => {
    const request = new HttpRequest('GET', 'http://localhost');
    const errorResponse = new HttpErrorResponse({ status: 401 });
    (httpHandler.handle as jasmine.Spy).and.returnValue(throwError(errorResponse));
    interceptor.intercept(request, httpHandler).subscribe(
      () => {},
      error => {
        expect(error).toBe(errorResponse);
        expect(router.navigate).toHaveBeenCalledWith(['/login']);
      }
    );
  });

  it('should rethrow the error without navigating to login when handle method throws an HttpErrorResponse with a status other than 401', () => {
    const request = new HttpRequest('GET', 'http://localhost');
    const errorResponse = new HttpErrorResponse({ status: 404 });
    (httpHandler.handle as jasmine.Spy).and.returnValue(throwError(errorResponse));
    interceptor.intercept(request, httpHandler).subscribe(
      () => {},
      error => {
        expect(error).toBe(errorResponse);
        expect(router.navigate).not.toHaveBeenCalled();
      }
    );
  });
});
