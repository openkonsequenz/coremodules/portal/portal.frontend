/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SessionContext } from '@common/session-context';
import { UserModule } from '@model/user-module';
import { AuthenticationService } from '@services/authentication.service';
import { UserModuleService } from '@services/user-module.service';
import { PGM_MODULE } from '@testing/user-module';
import { of } from 'rxjs';
import { ModuleGridComponent } from './module-grid.component';

describe('ModuleGridComponent', () => {
  let component: ModuleGridComponent;
  let fixture: ComponentFixture<ModuleGridComponent>;
  let userModuleService: UserModuleService;
  let authService: AuthenticationService;
  let sessionContext: SessionContext;

  beforeEach(async () => {
    const userModuleServiceMock = {
      getUserModulesForUser: jasmine.createSpy('getUserModulesForUser').and.returnValue(of([])),
    };

    const authServiceMock = {
      checkAuth: jasmine.createSpy('checkAuth').and.returnValue(of(true)),
    };

    const sessionContextMock = {
      getAccessToken: jasmine.createSpy('getAccessToken').and.returnValue('token'),
      getAccessTokenDecoded: jasmine.createSpy('getAccessTokenDecoded').and.returnValue('token'),
    };

    await TestBed.configureTestingModule({
      declarations: [ModuleGridComponent],
      providers: [
        { provide: UserModuleService, useValue: userModuleServiceMock },
        { provide: AuthenticationService, useValue: authServiceMock },
        { provide: SessionContext, useValue: sessionContextMock },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ModuleGridComponent);
    component = fixture.componentInstance;
    userModuleService = TestBed.inject(UserModuleService);
    authService = TestBed.inject(AuthenticationService);
    sessionContext = TestBed.inject(SessionContext);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call getUserModules on the userModuleService when ngOnInit is called', () => {
    component.ngOnInit();
    expect(userModuleService.getUserModulesForUser).toHaveBeenCalled();
  });

  it('should call checkAuth on the authService when goToModuleIfValid is called', () => {
    const mockUserModule = PGM_MODULE;
    component.goToModuleIfValid(mockUserModule);
    expect(authService.checkAuth).toHaveBeenCalled();
  });

  it('#getUserModules should get user modules', () => {
    const userModules = [new UserModule()];
    (userModuleService.getUserModulesForUser as jasmine.Spy).and.returnValue(of(userModules));
    spyOn(component, 'setModulesForRole');

    component.getUserModules();

    expect(userModuleService.getUserModulesForUser).toHaveBeenCalled();
    expect(component.setModulesForRole).toHaveBeenCalledWith(userModules);
  });

  it('#setModulesForRole should set user modules', () => {
    const userModules = [new UserModule()];
    userModules[0].requiredRole = 'testRole';
    (sessionContext.getAccessTokenDecoded as jasmine.Spy).and.returnValue({ roles: ['testRole'] });

    component.setModulesForRole(userModules);

    expect(component.userModules).toEqual(userModules);
    expect(component.moduleRights).toBeTruthy();
  });

  it('#setModulesForRole should throw error if no access token decoded', () => {
    (sessionContext.getAccessTokenDecoded as jasmine.Spy).and.returnValue(null);
    expect(() => component.setModulesForRole([])).toThrowError('No access token decoded found!');
  });

  it('should check auth when window gets focus', () => {
    (authService.checkAuth as jasmine.Spy).and.returnValue(of(null));
    const event = new Event('focus');
    window.dispatchEvent(event);
    expect(authService.checkAuth).toHaveBeenCalled();
  });
});
