/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { SessionContext } from '@common/session-context';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let httpTestingController: HttpTestingController;
  let router: Router;
  let sessionContext: SessionContext;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      declarations: [AppComponent],
      providers: [SessionContext],
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    httpTestingController = TestBed.inject(HttpTestingController);
    router = TestBed.inject(Router);
    sessionContext = TestBed.inject(SessionContext);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load settings on creation', () => {
    const mockSettings = { setting1: 'value1', setting2: 'value2' };
    httpTestingController.expectOne('assets/settings.json').flush(mockSettings);
    expect(sessionContext.settings).toEqual(mockSettings);
  });

  it('should navigate to login when centralHttpResultCode$ emits 401', () => {
    const navigateSpy = spyOn(router, 'navigate');
    sessionContext.centralHttpResultCode$.next(401);
    expect(navigateSpy).toHaveBeenCalledWith(['/login']);
  });
});
