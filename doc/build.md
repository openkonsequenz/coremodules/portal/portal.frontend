# Pre-Requirements:

- NodeJs (we use version 18.18.1)
- Chrome Browser

# Style Guide
https://wiki.openkonsequenz.de/Styleguide_Komponenten


# Project Installation
Run `npm install` in the roor order to install the project dependencies.

# Project Scripts

This project includes the following npm scripts for development and testing:

- `ng`: This script runs the Angular CLI command. You can use it to run any Angular CLI command by prefixing it with `npm run`. For example, `npm run ng build`.

- `start`: This script starts the development server. It uses a proxy configuration file (`proxy.conf.json`) and runs the server on port 4201.

- `build`: This script builds the application for production. It uses the production configuration.

- `test`: This script runs the unit tests using the Angular CLI's test command.

- `test:coverage`: This script runs the unit tests and generates a code coverage report. The report is saved in the `coverage/Chrome*/` folder.

- `test:coverage:headless`: This script runs the unit tests in a headless Chrome browser, generates a code coverage report, and does not watch for file changes.

- `lint`: This script runs the linter for checking the code quality.